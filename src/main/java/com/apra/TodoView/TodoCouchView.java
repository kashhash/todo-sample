package com.apra.TodoView;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.ektorp.CouchDbConnector;
import org.ektorp.ViewResult;
import org.ektorp.support.CouchDbRepositorySupport;
import org.ektorp.support.DesignDocument;
import org.ektorp.support.View;

import com.apra.controller.DeviceNameInfo;
import com.apra.controller.TODO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TodoCouchView extends CouchDbRepositorySupport<DeviceNameInfo>  {

	public TodoCouchView(CouchDbConnector db) {
		super(DeviceNameInfo.class, db);
		try {
			DesignDocument designDoc = getDesignDocumentFactory().getFromDatabase(db, "_design/DeviceNameInfo");
			if (designDoc != null && null != designDoc.getId()) {
				db.delete(designDoc);
				initStandardDesignDocument();
			}
		} catch (Exception ex) {
			// first time if doc does not exits
			initStandardDesignDocument();
		}
	}
	
	@View(name = "getDeviceNames", map = "function(doc){if(doc.type == 'DeviceNames'){emit(null,doc);}}")
	public DeviceNameInfo getDeviceNameInfo() {
		ViewResult r = db.queryView(createQuery("getDeviceNames"));
		HashMap<String, String> map = new HashMap<String, String>();
		if (r.getRows().size() == 0)
			return null;
		DeviceNameInfo d = null;
		for (int i = 0; i < r.getRows().size(); i++) {
			d = getDevice(r.getRows().get(i).getValue().toString());
			if(d!=null) {
				
			}
			return d;
		}
		return null;
	}
	
	private DeviceNameInfo getDevice(String devNameStr) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			DeviceNameInfo d = objectMapper.readValue(devNameStr, DeviceNameInfo.class);
			return d;
		} catch (JsonProcessingException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}
	
	public DeviceNameInfo saveDeviceNameInfo(DeviceNameInfo deviceNameInfo) {
		try {
			db.create(deviceNameInfo);

			return deviceNameInfo;
		} catch (Exception e) {
			return null;
		}
	}

}
